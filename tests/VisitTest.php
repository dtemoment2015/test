<?php

use Denis\Test\Visit;

class VisitTest extends PHPUnit\Framework\TestCase
{

   public function testVisit()
   {
      $visit = new Visit;

      $result = $visit->view();

      $this->assertEquals(true, $result);
   }
}
