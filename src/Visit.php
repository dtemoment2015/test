<?php

namespace Denis\Test;

use PDO;
use PDOException;

class Visit
{
   protected $table = 'visits';

   protected $pdo;

   public function __construct()
   {
      //ПОДКЛЮЧЕНИЕ PDO - MYSQL
      try {
         $env = parse_ini_file('env.ini');
         $this->pdo = new PDO(
            'mysql:host=' . $env['db_host'] . ';dbname=' . $env['db_name'],
            $env['db_user'],
            $env['db_password']
         );
      } catch (PDOException $e) {
         die('Ошибка подключения MySQL');
      }
   }

   public function view($params = [])
   {
      //views_count - при создании имеет по умолчанию 1
      //view_date - при создании имеет по умолчанию текущую дату

      $query = $this->pdo->prepare(

         "INSERT INTO `" . $this->table . "` (
                     `id`, 
                     `ip_address`, 
                     `user_agent`,
                     `page_url`
               )  VALUE (
                     :id, 
                     :ip_address, 
                     :user_agent,
                     :page_url
               ) 
               ON DUPLICATE KEY UPDATE 
                     views_count=views_count+1,
                     view_date=CURRENT_TIMESTAMP
          "
      );

      return $query->execute($params);
   }
}
