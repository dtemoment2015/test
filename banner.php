<?php
require __DIR__ . '/vendor/autoload.php';

use Denis\Test\Visit;

$img = 'test.png'; //Изображение

$visit = new Visit; //Инницилизация просмотра

if (file_exists($img)) {

   $ip_address = $_SERVER['REMOTE_ADDR'];
   $user_agent = $_SERVER['HTTP_USER_AGENT'];
   $page_url = $_SERVER['HTTP_REFERER'];

   $visit->view(
      [
         'id' => md5($ip_address . $user_agent . $page_url), //генерирую MD5-ХЭШ ID , чтобы использовать ON DUPLICATE KEY 
         'ip_address' => $ip_address,
         'user_agent' => $user_agent,
         'page_url' => $page_url
      ]
   );  //Просмотр


   $imgInfo = getimagesize($img);

   header('Content-Type: ' . $imgInfo['mime']);
   header('Content-Length: ' . filesize($img));

   readfile($img);
} else {
   header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
}

exit;
